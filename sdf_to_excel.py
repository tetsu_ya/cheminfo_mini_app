#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import shutil
from argparse import ArgumentParser
import xlsxwriter
from rdkit.Chem import SDMolSupplier, MolToMolBlock
from rdkit.Chem.rdMolDescriptors import CalcExactMolWt, CalcCrippenDescriptors, CalcTPSA
# from rdkit.Chem.Draw import MolToFile

col_num_width = 7
col_str_width = 45
col_prp_width = 14
row_height = 120
col_tag_width_plus = 1
insert_image_dict = {
    "x_offset": 15,
    "y_offset": 5
}


def get_arguments():
    parser = ArgumentParser()
    parser.add_argument("input_sdf")
    parser.add_argument("-o", "--output-excel", default="output.xlsx")
    parser.add_argument("-c", "--max-count", type=int, default=0)
    parser.add_argument("-p", "--add-properties", action="store_true", default=False)
    return parser.parse_args()


def sdf_to_excel(input_sdf_path, output_excel_path, max_count=0, add_properties=False):
    tmp_dir = "_tmp"
    if add_properties:
        start_tag_num = 3
    else:
        start_tag_num = 2

    if not os.path.isdir(tmp_dir):
        os.mkdir(tmp_dir)
    reader = SDMolSupplier(input_sdf_path)
    book = xlsxwriter.Workbook(output_excel_path)

    header_format = book.add_format()
    header_format.set_align("center")
    header_format.set_align("vcenter")
    header_format.set_bold()
    header_format.set_bottom(2)
    default_format = book.add_format()
    default_format.set_align("center")
    default_format.set_align("vcenter")
    default_format.set_top(1)
    default_format.set_bottom(1)
    default_format.set_text_wrap()

    sheet = book.add_worksheet("Sheet1")

    header_name_list = []
    header_length_list = []
    for count, mol in enumerate(reader):
        if count >= max_count > 0:
            break
        img_path = os.path.join(tmp_dir, "{}.png".format(count))
        # MolToFile(mol, img_path, (300, 150))
        mol_path = os.path.join(tmp_dir, "{}.mol".format(count))
        open(mol_path, "w").write(MolToMolBlock(mol))
        command = "obabel {} -O {} -xw 300 -xh 150 -xd".format(mol_path, img_path)
        os.system(command)
        sheet.write(count + 1, 0, count + 1, default_format)
        sheet.write_blank(count + 1, 1, None, default_format)
        sheet.insert_image(count + 1, 1, img_path, insert_image_dict)

        if add_properties:
            mw = CalcExactMolWt(mol)
            clogp = CalcCrippenDescriptors(mol)[0]
            tpsa = CalcTPSA(mol)
            prop_text = (
                "MW: {:.2f}\n"
                "CLogP: {:.2f}\n"
                "TPSA: {:.2f}"
                "".format(mw, clogp, tpsa)
            )
            sheet.write(count + 1, 2, prop_text, default_format)

        tag_list = mol.GetPropNames()
        for tag in tag_list:
            if tag not in header_name_list:
                header_name_list.append(tag)
                header_length_list.append(len(tag))
        for idx, (name, length) in enumerate(zip(header_name_list, header_length_list)):
            value = mol.GetProp(name)
            if length < len(value):
                header_length_list[idx] = len(value)
            sheet.write(count + 1, idx + start_tag_num, value, default_format)
        sheet.set_row(count + 1, row_height)

    sheet.write(0, 0, "Number", header_format)
    sheet.set_column(0, 0, col_num_width)
    sheet.write(0, 1, "Structure", header_format)
    sheet.set_column(1, 1, col_str_width)
    if add_properties:
        sheet.write(0, 2, "Prop", header_format)
        sheet.set_column(2, 2, col_prp_width)
    for idx, name in enumerate(header_name_list):
        sheet.write(0, idx + start_tag_num, name, header_format)
    for idx, length in enumerate(header_length_list):
        sheet.set_column(idx + start_tag_num, idx + start_tag_num, length + col_tag_width_plus)
    book.close()
    shutil.rmtree(tmp_dir)


def main():
    args = get_arguments()
    sdf_to_excel(args.input_sdf, args.output_excel, args.max_count, args.add_properties)
    #sdf_to_excel("piceatannol/Piceatannol-sim_matched2d.sdf", "piceatannol/Piceatannol-sim_matched2d.xlsx", 100)
    #sdf_to_excel("piceatannol/Piceatannol-sim_matched3d.sdf", "piceatannol/Piceatannol-sim_matched3d.xlsx", 100)


if __name__ == "__main__":
    main()
