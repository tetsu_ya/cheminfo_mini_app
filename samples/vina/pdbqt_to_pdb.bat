@echo off
set SCRIPT_DIR=%~dp0
set /p ADT_PATH=<%SCRIPT_DIR%\adt_path.txt
set ADT_PYTHONLIB_PATH=%ADT_PATH%\Lib\site-packages
set ADT_UTIL24_PATH=%ADT_PYTHONLIB_PATH%\AutoDockTools\Utilities24
set LD_LIBRARY_PATH=%ADT_PATH%\lib
"%ADT_PATH%\python" "%ADT_UTIL24_PATH%\pdbqt_to_pdb.py" %*
