# -*- coding: utf-8 -*-

from argparse import ArgumentParser
from rdkit.Chem import SDMolSupplier, SDWriter, MolToInchi


def get_arguments():
    parser = ArgumentParser()
    parser.add_argument("input_sdf_paths", nargs="*", help="input sdf path list")
    parser.add_argument("-o", "--output-sdf-path", type=str, default="output.sdf", help="output sdf path")
    return parser.parse_args()


def main():
    args = get_arguments()
    inchi_list = []
    writer = SDWriter(args.output_sdf_path)
    for sdf in args.input_sdf_paths:
        reader = SDMolSupplier(sdf)
        for mol in reader:
            inchi = MolToInchi(mol)
            if inchi not in inchi_list:
                inchi_list.append(inchi)
                writer.write(mol)
    writer.close()


if __name__ == "__main__":
    main()
