#!/usr/bin/env python
#  -*- coding: utf-8 -*-

"""
検索先のSDファイルとクエリとなるクエリとなる化合物のSDファイルを入力すると
"""

import sys
import os
from argparse import ArgumentParser
from time import time
from rdkit.Chem import AllChem
from rdkit.Chem.Pharm2D import Gobbi_Pharm2D, Generate
from rdkit import Chem, DataStructs

METHOD_TYPE = [
    "substructure",
    "2d_similarity",
    "3d_similarity"
]


def _get_arguments():
    parser = ArgumentParser(fromfile_prefix_chars='@')
    parser.add_argument("-q", "--query-path", required=True, help="query file (sdf or mol)")
    parser.add_argument("-t", "--target-path", nargs="*", required=True, help="target sd file")
    parser.add_argument("-m", "--match-method", required=True, choices=METHOD_TYPE,
                        help="match method ({})".format(", ".join(METHOD_TYPE)))
    parser.add_argument("-o", "--output-path", required=True, help="output sdf path")
    parser.add_argument("--similarity-threshold", type=float, default=0.0, help="similarity threshold")
    return parser.parse_args()


def substruct_match_sdf_to_sdf(
        input_query_sdf_path, input_target_sdf_path_list, output_sdf_path, end_count=0, print_count=1000):

    print("Substructure match start")
    start_time = time()
    all_count = 1
    query_mol = next(Chem.SDMolSupplier(input_query_sdf_path, removeHs=False))
    matched_mol_list = list()
    writer = Chem.SDWriter(output_sdf_path)
    for input_target_sdf_path in input_target_sdf_path_list:
        print("SDF: {}".format(input_target_sdf_path))
        reader = Chem.SDMolSupplier(input_target_sdf_path, removeHs=False)
        for count, mol in enumerate(reader):
            if all_count > end_count > 0:
                break
            if all_count % print_count == 0:
                sys.stdout.write("\rall_count: {:0>8}, match_count: {:0>5}".format(all_count, len(matched_mol_list)))
                sys.stdout.flush()
            if not mol:
                all_count += 1
                continue
            mol = Chem.AddHs(mol)
            if mol.HasSubstructMatch(query_mol):
                mol = Chem.RemoveHs(mol)
                matched_mol_list.append(mol)
                writer.write(mol)
            all_count += 1
    writer.close()
    print("")
    print("Substructure match end. (count={}, time={}sec)".format(len(matched_mol_list), time() - start_time))
    return matched_mol_list


def similarity_3d_match_sdf_to_sdf(
        input_query_sdf_path, input_target_sdf_path_list, output_similarity_sorted_sdf_path,
        threshold, end_count=0, print_count=10):
    print("Similarity match start")
    start_time = time()
    all_count = 1
    query_mol = next(Chem.SDMolSupplier(input_query_sdf_path, removeHs=False))
    query_mol = Chem.AddHs(query_mol)
    AllChem.EmbedMolecule(query_mol)
    AllChem.UFFOptimizeMolecule(query_mol, maxIters=200)
    query_mol = Chem.RemoveHs(query_mol)
    factory1 = Gobbi_Pharm2D.factory
    query_fp = Generate.Gen2DFingerprint(query_mol, factory1, dMat=Chem.Get3DDistanceMatrix(query_mol))

    matched_mol_list = list()
    for input_target_sdf_path in input_target_sdf_path_list:
        print("SDF: {}".format(input_target_sdf_path))
        reader = Chem.SDMolSupplier(input_target_sdf_path)
        for count, mol in enumerate(reader):
            if all_count > end_count > 0:
                break
            if all_count % print_count == 0:
                sys.stdout.write("\rall_count: {:0>8}".format(all_count))
                sys.stdout.flush()
            if not mol:
                all_count += 1
                continue
            try:
                mol1 = Chem.AddHs(mol)
                AllChem.EmbedMolecule(mol1)
                AllChem.UFFOptimizeMolecule(mol1, maxIters=200)
                mol1 = Chem.RemoveHs(mol1)
                fp2 = Generate.Gen2DFingerprint(mol1, factory1, dMat=Chem.Get3DDistanceMatrix(mol1))
                tani = DataStructs.TanimotoSimilarity(query_fp, fp2)
                if tani >= threshold:
                    mol.SetProp("Similarity", str(tani))
                    matched_mol_list.append([mol, tani])
            except ValueError as e:
                print(count, e)
                continue
            finally:
                all_count += 1
    print("")
    print("Similarity match end. (count={}, time={})".format(len(matched_mol_list), time() - start_time))
    print("Sort and write start")
    matched_mol_list.sort(key=lambda x: x[1], reverse=True)
    writer = Chem.SDWriter(output_similarity_sorted_sdf_path)
    for data in matched_mol_list:
        writer.write(data[0])
    writer.close()
    print("Sort and write end (time={}sec)".format(time() - start_time))
    return matched_mol_list


def similarity_2d_match_sdf_to_sdf(
        input_query_sdf_path, input_target_sdf_path_list, output_similarity_sorted_sdf_path,
        threshold, end_count=0, print_count=10):
    print("Similarity match start")
    start_time = time()
    all_count = 1
    query_mol = next(Chem.SDMolSupplier(input_query_sdf_path))
    query_fp = AllChem.GetMorganFingerprintAsBitVect(query_mol, 2)
    matched_mol_list = list()
    for input_target_sdf_path in input_target_sdf_path_list:
        print("SDF: {}".format(input_target_sdf_path))
        reader = Chem.SDMolSupplier(input_target_sdf_path)
        for count, mol in enumerate(reader):
            if all_count > end_count > 0:
                break
            if all_count % print_count == 0:
                sys.stdout.write("\r{:0>8}".format(all_count))
                sys.stdout.flush()
            if not mol:
                all_count += 1
                continue
            try:
                fp2 = AllChem.GetMorganFingerprintAsBitVect(mol, 2)
                tani = DataStructs.TanimotoSimilarity(query_fp, fp2)
                if tani >= threshold:
                    mol.SetProp("Similarity", str(tani))
                    matched_mol_list.append([mol, tani])
            except ValueError as e:
                print(count, e)
                continue
            finally:
                all_count += 1
    print("")
    print("Similarity match end. (count={}, time={})".format(len(matched_mol_list), time() - start_time))
    print("Sort and write start")
    matched_mol_list.sort(key=lambda x: x[1], reverse=True)
    writer = Chem.SDWriter(output_similarity_sorted_sdf_path)
    for data in matched_mol_list:
        writer.write(data[0])
    writer.close()
    print("Sort and write end")
    return matched_mol_list


def _main():
    args = _get_arguments()
    if args.match_method == METHOD_TYPE[0]:
        substruct_match_sdf_to_sdf(args.query_path, args.target_path, args.output_path)
    elif args.match_method == METHOD_TYPE[1]:
        similarity_2d_match_sdf_to_sdf(args.query_path, args.target_path, args.output_path, args.similarity_threshold)
    elif args.match_method == METHOD_TYPE[2]:
        similarity_3d_match_sdf_to_sdf(args.query_path, args.target_path, args.output_path, args.similarity_threshold)


def _test():
    output_dir = "piceatannol"
    subst_query_sdf_path = os.path.join(output_dir, "Piceatannol-core.sdf")
    query_sdf_path = os.path.join(output_dir, "Piceatannol.sdf")
    target_sdf_path_list = [
        "F:/Namiki_Database/Namiki_201704_0001.sdf",
        "F:/Namiki_Database/Namiki_201704_0002.sdf",
        "F:/Namiki_Database/Namiki_201704_0003.sdf",
        "F:/Namiki_Database/Namiki_201704_0004.sdf",
        "F:/Namiki_Database/Namiki_201704_0005.sdf",
        "F:/Namiki_Database/Namiki_201704_0006.sdf",
        "F:/Namiki_Database/Namiki_201704_0007.sdf",
        "F:/Namiki_Database/Namiki_201704_0008.sdf",
        "F:/Namiki_Database/Namiki_201704_0009.sdf",
        "F:/Namiki_Database/Namiki_201704_0010.sdf",
        "F:/Namiki_Database/Namiki_201704_0011.sdf",
        "F:/Namiki_Database/Namiki_201704_0012.sdf",
        "F:/Namiki_Database/Namiki_201704_0013.sdf",
        "F:/Namiki_Database/Namiki_201704_0014.sdf"
    ]
    subst_matched_sdf_path = os.path.join(output_dir, "Piceatannol-subst_matched.sdf")
    sim3_matched_sdf_path = os.path.join(output_dir, "Piceatannol-sim_matched3d.sdf")
    sim2_matched_sdf_path = os.path.join(output_dir, "Piceatannol-sim_matched2d.sdf")
    substruct_match_sdf_to_sdf(subst_query_sdf_path, target_sdf_path_list, subst_matched_sdf_path)
    #similarity_3d_match_sdf_to_sdf(query_sdf_path, [subst_matched_sdf_path], sim3_matched_sdf_path)
    #similarity_2d_match_sdf_to_sdf(query_sdf_path, [subst_matched_sdf_path], sim2_matched_sdf_path)


if __name__ == "__main__":
    _main()
