#!/usr/bin/env python
#  -*- coding: utf-8 -*-

u"""
このスクリプトはナミキディスクをコピーしたフォルダにて実行することを想定しています。
"""

import os
import zipfile
from rdkit import Chem


def sdf_zips_to_smi(dir_path):
    interval = 10000
    count = 0
    miss_count = 0
    basename = os.path.basename(dir_path)
    ouf = open(os.path.join(dir_path, basename + ".smi"), "w")
    for item1 in os.listdir(dir_path):
        basename1, ext = os.path.splitext(item1)
        if ext == ".zip" and "New" not in basename1:
            inz = zipfile.ZipFile(os.path.join(dir_path, item1))
            for inz_filename in inz.namelist():
                inf = inz.open(inz_filename, "r")
                supplier = Chem.ForwardSDMolSupplier(inf)
                while True:
                    try:
                        mol = supplier.next()
                    except StopIteration:
                        break
                    if not mol:
                        miss_count += 1
                        continue
                    ns_code = mol.GetProp("NScode")
                    if ns_code[-5:] != "-0000":
                        miss_count += 1
                        continue
                    ns_code = ns_code[:-5]
                    smiles = Chem.MolToSmiles(mol)
                    ouf.write("{smiles}\t{code}\n".format(code=ns_code, smiles=smiles))
                    if count % interval == 0:
                        print("dir: {dir}, zip:{zip} , count: {count}".format(dir=dir_path, zip=item1, count=count))
                    count += 1
                print("")
                inf.close()
            inz.close()
    ouf.close()
    os.rename(os.path.join(dir_path, basename + ".smi"),
              os.path.join(dir_path, basename + "_num{}_miss{}.smi".format(count, miss_count)))


def main():
    work_dir = os.getcwd()
    for item1 in os.listdir(work_dir):
        if os.path.isdir(item1):
            sdf_zips_to_smi(os.path.join(work_dir, item1))


if __name__ == "__main__":
    main()
