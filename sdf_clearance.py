#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os import path
from sys import stdout
from zipfile import ZipFile
from argparse import ArgumentParser
from rdkit.Chem import SDMolSupplier, SDWriter, ForwardSDMolSupplier

PRINT_COUNT = 1000


def get_arguments():
    parser = ArgumentParser(fromfile_prefix_chars="@")
    parser.add_argument("input_files", nargs="*", help="input file path. (sdf, zip)")
    parser.add_argument("-o", "--output-sdf", default="output.sdf", help="output file path. (sdf)")
    parser.add_argument("-m", "--max-count", type=int, default=0, help="max count")
    return parser.parse_args()


def main():
    args = get_arguments()
    all_count = 0
    max_count = args.max_count
    input_files = args.input_files
    output_sdf = args.output_sdf

    def sdf_to_sdf(reader1, writer1, all_count1, max_count1):
        for mol in reader1:
            if all_count1 >= max_count1 > 0:
                break
            if all_count1 % PRINT_COUNT == 0:
                stdout.write("\rCount: {}".format(all_count1))
            if mol:
                writer1.write(mol)
            all_count1 += 1
        return all_count1

    writer = SDWriter(output_sdf)
    for input_file in input_files:
        input_extension = path.splitext(input_file)[1]
        if input_extension == ".zip":
            with ZipFile(input_file, "r") as inf:
                for sdf_file in inf.filelist:
                    with inf.open(sdf_file, "r") as inf2:
                        reader = ForwardSDMolSupplier(inf2)
                        all_count = sdf_to_sdf(reader, writer, all_count, max_count)
        elif input_extension == ".sdf":
            reader = SDMolSupplier(input_file)
            all_count = sdf_to_sdf(reader, writer, all_count, max_count)
        else:
            raise Exception("input file type is invalid. ({})".format(input_file))
    writer.close()


if __name__ == "__main__":
    main()