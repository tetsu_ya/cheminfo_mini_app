#!/usr/bin/env python
#  -*- coding: utf-8 -*-

import argparse
import os
import json
import csv
import re


def get_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("input_csv_path")
    parser.add_argument("mol_dir_path", default=".")
    parser.add_argument("output_sdf_path")
    parser.add_argument("-j", "--conf-json-path")
    parser.add_argument("-f", "--force-output", action="store_true", default=False)
    parser.add_argument("-r", "--rdkit-check", action="store_true", default=False)
    parser.add_argument("-e", "--output-encoding", default="utf-8")
    parser.add_argument("-s", "--add-serial-tag", nargs=3)
    return parser.parse_args()


def load_csv(input_csv_path, conf_dict):
    comp_list = []
    with open(input_csv_path, "r") as inf:
        reader = csv.DictReader(inf)
        for row in reader:
            data_dict = {}
            data_length = 0
            for key, value in conf_dict.items():
                try:
                    data_length += len(row[key])
                    data_dict[value] = row[key]
                except KeyError:
                    data_dict[value] = ""
            if not data_length:
                continue
            comp_list.append(data_dict)
    return comp_list


def load_mols(mol_dir_path, rdkit_check=False):
    if rdkit_check:
        from rdkit import Chem
    mol_dict = {}
    for item in os.listdir(mol_dir_path):
        if os.path.splitext(item)[1] != ".mol":
            continue
        with open(os.path.join(mol_dir_path, item), "r") as inf:
            mol_block = inf.read()
        match = re.match("(.+\nM\s+END)", mol_block, re.DOTALL)
        if match:
            if rdkit_check:
                mol = Chem.MolFromMolBlock(match.group(1))
                if not mol:
                    print("mol file is invalid(rdkit check). ({})".format(item))
                    continue
        else:
            print("mol file is invalid. ({})".format(item))
            continue
        mol_dict[item] = match.group(1) + "\n"
    return mol_dict


def write_sdf(output_sdf_path, comp_list, mol_dict, serial_tag_name, serial_tag_format, serial_tag_start, encoding):
    serial_count = serial_tag_start
    with open(output_sdf_path, "wb") as ouf:
        for count, comp in enumerate(comp_list):
            try:
                ouf.write(mol_dict[comp["_molfile"]].encode(encoding))
            except KeyError:
                print("mol filename not correspond. ({})".format(comp["_molfile"]))
                serial_count += 1
                continue
            if serial_tag_name:
                serial_tag_value = serial_tag_format.format(serial_count)
                ouf.write(">  <{}>  ({}) \n{}\n\n".format(
                    serial_tag_name, count + 1, serial_tag_value).encode(encoding))
            for key, value in comp.items():
                if key == "_molfile":
                    continue
                ouf.write(">  <{}>  ({}) \n{}\n\n".format(key, count + 1, value).encode(encoding))
            ouf.write("$$$$\n".encode(encoding))
            serial_count += 1


def main():
    args = get_arguments()
    input_csv_path = args.input_csv_path
    output_sdf_path = args.output_sdf_path
    mol_dir_path = args.mol_dir_path
    rdkit_check = args.rdkit_check
    output_encoding = args.output_encoding
    if args.add_serial_tag and len(args.add_serial_tag) == 3:
        serial_tag_name = args.add_serial_tag[0]
        serial_tag_format = args.add_serial_tag[1]
        serial_tag_start = int(args.add_serial_tag[2])
    else:
        serial_tag_name = None
        serial_tag_format = None
        serial_tag_start = 0
    program_name = os.path.splitext(os.path.basename(__file__))[0]
    if not args.conf_json_path:
        conf_json_path = program_name + ".json"
    else:
        conf_json_path = args.conf_json_path
    conf = json.loads(open(conf_json_path, "rb").read().decode("utf-8"))
    if not os.path.isfile(input_csv_path):
        print("CSV file is not found.")
        exit(1)
    if not os.path.isdir(mol_dir_path):
        print("Directory of mol files is not found.")
        exit(1)
    if os.path.isfile(output_sdf_path) and not args.force_output:
        print("Output sdf is found.")
        exit(1)

    comp_list = load_csv(input_csv_path, conf)
    mol_dict = load_mols(mol_dir_path, rdkit_check)
    write_sdf(output_sdf_path, comp_list, mol_dict, serial_tag_name,
              serial_tag_format, serial_tag_start, output_encoding)


if __name__ == "__main__":
    main()
