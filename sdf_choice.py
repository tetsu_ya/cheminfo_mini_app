# -*- coding: utf-8 -*-

from argparse import ArgumentParser
from rdkit.Chem import SDMolSupplier, SDWriter


def get_arguments():
    parser = ArgumentParser()
    parser.add_argument("input_sdf_path", type=str, help="input sdf path")
    parser.add_argument("input_list_path", type=str, help="input list text path")
    parser.add_argument("tag_name", type=str, help="tag name corresponding sdf")
    parser.add_argument("-o", "--output_sdf_path", type=str, help="output sdf path")
    return parser.parse_args()


def main():
    args = get_arguments()
    id_list = open(args.input_list_path, "r").read().split("\n")
    id_list.remove("")

    reader = SDMolSupplier(args.input_sdf_path)
    writer = SDWriter(args.output_sdf_path)
    for mol in reader:
        id_value = mol.GetProp(args.tag_name)
        if id_value in id_list:
            writer.write(mol)
    writer.close()


if __name__ == "__main__":
    main()
