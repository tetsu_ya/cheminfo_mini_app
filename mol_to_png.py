#!/usr/bin/env python
#  -*- coding: utf-8 -*-

import os
import argparse

from rdkit import Chem
from rdkit.Chem import Draw


def get_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("input_path", help="mol file or directory path.")
    parser.add_argument("-o", "--output-dir", default=".", help="output directory.")
    parser.add_argument("-d", "--directory", action="store_true", default=False, help="path is directory?")
    return parser.parse_args()


def mol_to_png(input_path, output_dir):
    basename = os.path.splitext(os.path.basename(input_path))[0]
    mol_block = open(input_path, "rb").read().decode("utf-8")
    mol = Chem.MolFromMolBlock(mol_block)
    Draw.MolToFile(mol, os.path.join(output_dir, basename + ".png"))


def main():
    args = get_arguments()
    if args.directory:
        for item in os.listdir(args.input_path):
            if os.path.splitext(item)[1] == ".mol":
                mol_to_png(os.path.join(args.input_path, item), args.output_dir)
    else:
        mol_to_png(args.input_path, args.output_dir)


if __name__ == "__main__":
    main()
